#!/usr/bin/bash
## Written By Eslam Shapsough
## RHEL/Centos 7.x Tested
echo "####################################"
echo "## Installing Python3 & Python3-pip....."
yum install python3 -y > /dev/null
yum install python3-pip -y > /dev/null
echo "## Upgrading pip....."
pip3 install --upgrade pip > /dev/null
rm -f /usr/bin/pip3 > /dev/null
ln -s /usr/local/bin/pip3 /usr/bin/pip > /dev/null
ln -s /usr/local/bin/pip3 /usr/bin/pip3 > /dev/null
echo "####################################"
echo "## Adding EPEL Repo List....."
yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm -y > /dev/null
yum check-update > /dev/null
yum update -y > /dev/null
echo "## Base System Update Done!"
echo "####################################"
echo "## Installing Ansible....."
yum install ansible -y > /dev/null
echo "####################################"
echo "## Installing git, nano, htop, bash-completion....."
yum install git nano htop bash-completion -y > /dev/null
echo "####################################"
echo "## Checking Ansible Install....."
ansible --version > /dev/null || echo "Install Failed!"
echo "####################################"
echo "## Generating id_rsa Keys....."
ssh-keygen -t rsa -N "" -f /root/.ssh/id_rsa
sed -i 's/#host_key_checking = False/host_key_checking = False/g' /etc/ansible/ansible.cfg
echo "####################################"
echo "####################################"
echo "####################################"
echo "####################################"
echo "## All Done! ##"
echo "####################################"
echo "####################################"
echo "####################################"
echo "####################################"
