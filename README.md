# Centos-Kubernetes
Kubernetes Setup On Centos 7.7

**To Setup:**

1- On Ansible Server "chmod +x shell/ansible-master.sh && ./shell/ansible-master.sh"

**Notes:**

- you MUST be root.
- check all ansible playbook 0002 file and modify docker regitrey and kubernetes namspace to match yours

---------------------------------------------------

**On Ansible Master Server:**

Edit "ansible/config/hosts" And Add All Servers IPs According To The Follwoing:
  
  A- [all] : All Cluster IPs Excluding Ansible Master Server.
  
  B- [kube_cluser] : All Kubernetes Machines IPs Excluding NFS Server & Ansible Master Server.
  
  C- [kube_master] : Only Kubernetes Master Node IP.
  
  D- [kube_slaves] : Kubernetes Slaves IPs Only, Excluding Ansible Master Server, and Kubernetes Master.

---------------------------------------------------

3- Set Back And Relax :)
